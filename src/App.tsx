import { Divider } from "antd";
import Style from "./App.module.scss";
import TabelContainer from "./Container/TabelContainer/TabelContainer";
import FormContainer from "./Container/FormContainer/FormContainer";
const App = () => {
  return (
    <div className={Style.BodyHolder}>
      <FormContainer/>
      <Divider orientation="left" >
        User List
      </Divider>
      <TabelContainer/>
    </div>
  );
};

export default App;
