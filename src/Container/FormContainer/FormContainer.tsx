import AddUserForm from '../../Components/Forms/AddUserForm/AddUserForm'
import Style from './FormContainer.module.scss'
const FormContainer = () => {
  return (
    <div className={Style.ContainerHolder}>
        <h3>Add User</h3>
        <AddUserForm/>
    </div>
  )
}

export default FormContainer