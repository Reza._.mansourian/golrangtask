import { useMutation, useQuery } from "react-query";
import UserTabel from "../../Components/Table/UserTabel";
import Style from "./TabelContainer.module.scss";
import { DeleteUserData, fetchData } from "../../API/APIServices";
import ErrorApi from "../../Components/Errors/ErrorApi/ErrorApi";
import LoadingComponents from "../../Components/LoadingComponents/LoadingComponents";
import { UseUserInfo } from "../../Components/Context/UserContext";
import { useEffect, useState } from "react";
import { Button, Modal } from "antd";
import { TabelUserDataType } from "../../Types/TabelUserDataType";
import { UserType } from "../../Types/UserType";
import { toast } from "react-toastify";
import { userDetailIntitalValue } from "./IntialValue";
import EditUserForm from "../../Components/Forms/EditUserForm/EditUserForm";
import UserSelect from "../../Components/Select/UserSelect";
import { GlobalSearch } from "../../Components/Search/GlobalSearch";

const TabelContainer = () => {
  const [removeModalState, setRemoveModalState] = useState<boolean>(false);
  const [editModalState, setEditModalState] = useState<boolean>(false);
  const [userDetailState, setUserDetailState] = useState<TabelUserDataType>(
    userDetailIntitalValue
  );
  const { UserList = [], setUserList } = UseUserInfo();
  const { data, isError, isLoading, error } = useQuery(["user"], () =>
    fetchData("users")
  );

  useEffect(() => {
    if (data) {
      setUserList(data);
    }
  }, [data]);

  const modalHnadler = (e: TabelUserDataType, type: number) => {
    if (type === 1) {
      setEditModalState(true);
    } else {
      setRemoveModalState(true);
    }
    setUserDetailState(e);
  };
  const mutation = useMutation(
    (userData: TabelUserDataType) => DeleteUserData(`users/${userData.key}`),
    {
      onSuccess: () => {
        toast("User Deleted!");
        const UsersListArray: UserType[] = [...UserList];
        const newUserListArray = UsersListArray.filter(
          (e) => e.id !== userDetailState?.key
        );
        setUserList(newUserListArray);
        setRemoveModalState(false);
      },
      onError: () => {
        toast.error("Somthing Went Wrong!");
      },
    }
  );

  const removeBrandHandler = async () => {
    try {
      await mutation.mutateAsync(userDetailState);
    } catch (error) {
      console.error("Error submitting data:", error);
    }
  };

  const FilterUserSelect = (id: number | string) => {
    if (id === "All") {
      setUserList(data);
      return;
    }
    const userListArray: UserType[] = [...data];
    const SelectedUser = userListArray.filter((user) => user.id === id);
    setUserList(SelectedUser);
  };

  return (
    <div className={Style.ContainerHolder}>
      <Modal
        title="Are you sure?"
        open={removeModalState}
        onCancel={() => setRemoveModalState(false)}
        destroyOnClose={true}
        footer={null}
      >
        <div className={Style.RemoveModalHolder}>
          <Button
            onClick={() => removeBrandHandler()}
            style={{ background: "green", color: "white" }}
          >
            Yes
          </Button>
          <Button
            type="primary"
            danger
            onClick={() => setRemoveModalState(false)}
          >
            No
          </Button>
        </div>
      </Modal>
      <Modal
        title="EditUser"
        open={editModalState}
        onCancel={() => setEditModalState(false)}
        destroyOnClose={true}
        footer={null}
      >
        <EditUserForm
          initialData={userDetailState}
          closeModal={() => setEditModalState(false)}
        />
      </Modal>

      <div className={Style.FilterHolder}>
        <UserSelect dataList={data} UserSelectId={FilterUserSelect} />
        <GlobalSearch dataList={data}/>
      </div>

      {isError && (
        <ErrorApi
          title={(error as Error)?.name ?? "Unknown"}
          description={(error as Error)?.message ?? "Unknown"}
        />
      )}

      {isLoading && <LoadingComponents />}

      {data && <UserTabel openModal={modalHnadler} UserList={UserList} />}
    </div>
  );
};

export default TabelContainer;
