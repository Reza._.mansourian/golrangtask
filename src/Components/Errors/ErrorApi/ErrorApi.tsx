import { Alert } from "antd";
import { ErrorApiType } from "../../../Types/ErrorApiTypes";

const ErrorApi = ({ title, description }: ErrorApiType) => {
  return (
    <Alert message={title} description={description} type="error" showIcon />
  );
};

export default ErrorApi;
