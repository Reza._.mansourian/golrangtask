import { UserType } from "../../Types/UserType";
import { UseUserInfo } from "../Context/UserContext";
import Style from "./GlobalSearch.module.scss";

type ComponentProp={
  dataList:UserType[]
}

export const GlobalSearch:React.FC<ComponentProp> = ({dataList}) => {
  const {  setUserList } = UseUserInfo();

  const search = (searchText: string) => {
    const userListArray: UserType[] = [...dataList];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const FilterData = userListArray.filter((user:any) => {
      return Object.keys(user).some((key) => {
        const value = user[key].toString().toLowerCase();
        const includes = value.includes(searchText.toLowerCase());
        return includes;
      });
    });
    setUserList(FilterData)
  };
  return (
    <div className={Style.SearchHolder}>
      <input
        type="search"
        placeholder="Search..."
        onChange={(e) => search(e.target.value)}
      />
      
    </div>
  );
};
