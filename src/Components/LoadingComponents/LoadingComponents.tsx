import { Spin } from "antd";
import Style from './LoadingComponent.module.scss'
const LoadingComponents = () => {
  return (
    <section className={Style.componentHolder}>
      <Spin tip="Loading" size="large">
      </Spin>
      <h2>Loading</h2>
    </section>
  );
};

export default LoadingComponents;
