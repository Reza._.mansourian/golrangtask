import { Select } from "antd";
import { UserType } from "../../Types/UserType";
import Style from "./UserTabelSelect.module.scss";
const { Option } = Select;

type ComponentProps = {
  dataList: UserType[];
  UserSelectId: (id: number) => void;
};

const UserSelect: React.FC<ComponentProps> = ({ dataList, UserSelectId }) => {
  return (
    <Select
      className={Style.SelectHolder}
      size="large"
      showSearch
      placeholder="select a name"
      optionFilterProp="children"
      loading={dataList?.length === 0}
      onChange={UserSelectId}
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      filterOption={(input: any, option) =>
        (option?.children ?? "").includes(input)
      }
    >
      <Option key={"All"} style={{ textAlign: "center" }} value={"All"}>
        {"AllUser"}
      </Option>
      {dataList?.map((item) => {
        return (
          <Option key={item.id} style={{ textAlign: "center" }} value={item.id}>
            {item.name}
          </Option>
        );
      })}
    </Select>
  );
};

export default UserSelect;
