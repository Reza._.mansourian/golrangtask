import { Field, Form, Formik } from "formik";
import { UserInititalValue } from "./InitialValues";
import Style from "./AddUserForm.module.scss";
import { useMutation } from "react-query";
import { postUserData } from "../../../API/APIServices";
import { toast } from "react-toastify";
import { UseUserInfo } from "../../Context/UserContext";
import { UserType } from "../../../Types/UserType";
import { validationSchema } from "./ValidationSchema";
import { useState } from "react";
import { Spin } from "antd";

const AddUserForm = () => {
  const[loadinglButton,setLoadinglButton]=useState<boolean>(false)
  const { UserList = [], setUserList } = UseUserInfo();
  const mutation = useMutation((userData) => postUserData(userData, "users"), {
    onSuccess: (data) => {
      toast("User Added!");
      const Users: UserType[] = [...UserList];
      Users.unshift(data);
      setUserList(Users);
      setLoadinglButton(false)
    },
    onError:()=>{
      toast.error("Somthing went Wrong!")
      setLoadinglButton(false)
    }
  });
  return (
    <Formik
      initialValues={UserInititalValue}
      validationSchema={validationSchema}
      onSubmit={async (values) => {
        setLoadinglButton(true)
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const info: any = {
          id: Math.floor(Math.random() * 1000),
          name: values.name,
          username: values.username,
          phone: values.phone,
          email: values.email,
          address: {
            city: values.city,
            zipcode: values.zipcode,
          },
        };

        try {
          await mutation.mutateAsync(info);
        } catch (error) {
          console.error(error);
        }
      }}
    >
      {({ errors, touched }) => (
        <Form className={Style.FormHolder}>
          <div className={Style.InputHolder}>
            <label htmlFor="name">Name</label>
            <Field id="name" name="name" placeholder="name" />
            {errors.name && touched.name && (
              <p className={Style.ErrorMessage}>{errors.name}</p>
            )}
          </div>

          <div className={Style.InputHolder}>
            <label htmlFor="username">username</label>
            <Field id="username" name="username" placeholder="username" />
            {errors.username && touched.username && (
              <p className={Style.ErrorMessage}>{errors.username}</p>
            )}
          </div>
          <div className={Style.InputHolder}>
            <label htmlFor="phone">Phone</label>
            <Field id="phone" name="phone" placeholder="phone" />
            {errors.phone && touched.phone && (
              <p className={Style.ErrorMessage}>{errors.phone}</p>
            )}
          </div>
          <div className={Style.InputHolder}>
            <label htmlFor="email">Email</label>
            <Field id="email" name="email" placeholder="email" />
            {errors.email && touched.email && (
              <p className={Style.ErrorMessage}>{errors.email}</p>
            )}
          </div>
          <div className={Style.InputHolder}>
            <label htmlFor="city">City</label>
            <Field id="city" name="city" placeholder="city" />
            {errors.city && touched.city && (
              <p className={Style.ErrorMessage}>{errors.city}</p>
            )}
          </div>
          <div className={Style.InputHolder}>
            <label htmlFor="zipcode">ZipCode</label>
            <Field id="zipcode" name="zipcode" placeholder="zipcode" />
            {errors.zipcode && touched.zipcode && (
              <p className={Style.ErrorMessage}>{errors.zipcode}</p>
            )}
          </div>
          <button type="submit" className={Style.CreateButton} disabled={loadinglButton}>
            {loadinglButton?<Spin/>:"Submit"}
          </button>
        </Form>
      )}
    </Formik>
  );
};

export default AddUserForm;
