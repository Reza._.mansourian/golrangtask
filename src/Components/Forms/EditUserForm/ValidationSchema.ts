import * as Yup from 'yup'

export const validationSchema=Yup.object().shape(
    {
        name: Yup.string().required('You Forgot this Field! '),
        username: Yup.string().required('You Forgot this Field! '),
        email: Yup.string().required('You Forgot this Field! '),
        city:Yup.string().required('You Forgot this Field! '),
        zipcode: Yup.string().required('You Forgot this Field! '),
        phone: Yup.string().required('You Forgot this Field! '),
    }
)