import React, { useState, useContext,ReactNode } from "react";
import { UserType } from "../../Types/UserType";

export const UserInfo = React.createContext<{
    UserList: UserType[] | undefined;
    setUserList: (value: UserType[]) => void;
  } | null>(null);


export const UseUserInfo = () => {
  const pc = useContext(UserInfo);
  if (pc === null) {
    throw new Error("useRefetchState Must be inside of Provider");
  }
  return pc;
};
interface UserInfoProviderProps {
    children: ReactNode;
  }
const UserInfoProvider= ({ children }:UserInfoProviderProps) => {
  //   const storedUserList = localStorage.getItem('userListSaved');
  //   const initialUserList: UserType[] = storedUserList
  // ? JSON.parse(storedUserList) as UserType[]
  // : [];
  
  const [UserList, setUserList] = useState<UserType[]>();

  // const closeIt=()=>{
  //   localStorage.setItem('userListSaved',JSON.stringify(UserList))
  // } 
  // window.onbeforeunload = closeIt;

  return (
    <UserInfo.Provider
      value={{
        UserList,
        setUserList: (value:UserType[]) => {
            setUserList(value)
        },
      }}
    >
      {children}
    </UserInfo.Provider>
  );
};

export { UserInfoProvider };
