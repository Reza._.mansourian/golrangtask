import React, { useRef, useState } from "react";
import { EditFilled, SearchOutlined,DeleteOutlined } from "@ant-design/icons";
import type { GetRef, TableColumnsType, TableColumnType } from "antd";
import { Button, Input, Space, Table } from "antd";
import type { FilterDropdownProps } from "antd/es/table/interface";
import Style from "./UserTabel.module.scss";
import Highlighter from "react-highlight-words";
import { TabelUserDataType } from "../../Types/TabelUserDataType";
import { UserType } from "../../Types/UserType";

type InputRef = GetRef<typeof Input>;



type DataIndex = keyof TabelUserDataType;

type ComponentProps = {
  UserList:UserType[]
  openModal: (e: TabelUserDataType, type: number) => void;
};

const UserTabel: React.FC <ComponentProps> = ({UserList,openModal}) => {
  const data: TabelUserDataType[] = [];

  UserList?.forEach((element) => {
    data.push({
      key: element.id,
      name: element.name,
      username: element.username,
      city: element.address.city,
      email: element.email,
      phone: element.phone,
      zipcode: element.address.zipcode,
    });
  });

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef<InputRef>(null);

  const handleSearch = (
    selectedKeys: string[],
    confirm: FilterDropdownProps["confirm"],
    dataIndex: DataIndex
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText("");
  };

  const getColumnSearchProps = (
    dataIndex: DataIndex
  ): TableColumnType<TabelUserDataType> => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div style={{ padding: 8 }} onKeyDown={(e) => e.stopPropagation()}>
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearch(selectedKeys as string[], confirm, dataIndex)
          }
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() =>
              handleSearch(selectedKeys as string[], confirm, dataIndex)
            }
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            close
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined style={{ color: filtered ? "#1677ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes((value as string).toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current, 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns: TableColumnsType<TabelUserDataType> = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: "12%",
      ...getColumnSearchProps("name"),
    },
    {
      title: "username",
      dataIndex: "username",
      key: "username",
      width: "12%",
      ...getColumnSearchProps("username"),
    },
    {
      title: "Phone",
      dataIndex: "phone",
      key: "Phone",
      width: "12%",
      ...getColumnSearchProps("phone"),
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "Email",
      width: "12%",
      ...getColumnSearchProps("email"),
    },
    {
      title: "city",
      dataIndex: "city",
      key: "city",
      width: "10%",
      ...getColumnSearchProps("city"),
    },
    {
      title: "zipcode",
      dataIndex: "zipcode",
      key: "zipcode",
      width: "20%",
      ...getColumnSearchProps("zipcode"),
    },
    {
      title: "Actions",
      dataIndex: "",
      width: "15%",
      key: "x",
      render: (value) => (
        <>
          <Button
            icon={<EditFilled />}
            style={{ background: "orange" }}
            type="primary"
            onClick={() => openModal(value, 1)}
          />
          <Button
            icon={<DeleteOutlined />}
            style={{marginLeft:'0.5rem'}}
            type="primary"
            danger
            onClick={() => openModal(value, 2)}
          />
        </>
      ),
    },
  ];

  return (
    <div className={Style.TabelHolder}>
      <Table
        columns={columns}
        dataSource={data}
        pagination={{
          position: ["bottomCenter"],
          pageSize:20
        }}
        
      />
    </div>
  );
};

export default UserTabel;
