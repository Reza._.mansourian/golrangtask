export type ErrorApiType = {
  title: string;
  description: string;
};
