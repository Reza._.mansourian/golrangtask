export type TabelUserDataType= {
    key: number;
    name: string;
    username: string;
    city: string;
    email: string;
    phone: string;
    zipcode: string;
  }