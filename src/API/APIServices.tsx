import axios from "axios";

export const fetchData = async (link:string) => {
    const { data } = await axios.get(`https://jsonplaceholder.typicode.com/${link}`);
    return data;
  };
// eslint-disable-next-line @typescript-eslint/no-explicit-any
  export const postUserData = async (userData:any,link:string) => {
      const response = await axios.post(`https://jsonplaceholder.typicode.com/${link}`, userData);
      return response.data;

  };
    export const DeleteUserData = async (link:string) => {
      const response = await axios.delete(`https://jsonplaceholder.typicode.com/${link}`);
      return response.data;

  };
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  export const UpdateUserData = async (userData:any,link:string) => {
    const response = await axios.put(`https://jsonplaceholder.typicode.com/${link}`, userData);
      return response.data;

  };