import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import { QueryClient, QueryClientProvider } from "react-query";
import { UserInfoProvider } from "./Components/Context/UserContext.tsx";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
const queryClient = new QueryClient({
  defaultOptions:{
    queries:{
      refetchOnWindowFocus:false
    },
  }
});

ReactDOM.createRoot(document.getElementById("root")!).render(
  <QueryClientProvider client={queryClient}>
    <UserInfoProvider>
    <React.StrictMode>
      <App />
      <ToastContainer />
    </React.StrictMode>
    </UserInfoProvider>
  </QueryClientProvider>
    ,
);
